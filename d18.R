library(qgam)
library(mgcViz)
library(tidyverse)
library(gratia)
library(dsm)#vis.concurvity
library(broom)
library(gridExtra)
library(sjPlot)
library(ggplotify)
library(parallel) 
ncores <- detectCores(logical = FALSE) 
cl <- makeForkCluster(ncores)
# install.packages("devtools")
#devtools::install_github("m-clark/visibly")
library(visibly)

dat <- dat.all.countries
dat <- dat.select.countries#
dat <- dat %>% mutate(N=n_nh4+n_no3)

dropdat <- drop_na(dat)
dropdat$ID_site <- as_factor(as.character(dropdat$ID_site))
#N####
N.sel <-
  qgam(N.y ~ s(n_nh4, bs = "tp") +
         s(n_no3, bs = "tp") +
         ti(n_nh4, n_no3, bs = "tp") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         s(mean_temp,bs= "tp")+
         s(mean_precip,bs="tp")+
         s(mean_age,bs ="tp",k=3)+ 
         as.factor(tree)+
         s(survey_year, bs="tp")+
         s(ID_site, bs="re"),
         #te(longitude,latitude,survey_year,bs="ts"),
         cluster = cl,
         data = dropdat , qu = 0.5)

summary(N.sel)         
b <- getViz(N.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
cqcheckp(b)
gam.check(b)
vis.concurvity(b)

library(itsadug)
dropdat <- drop_na(dat)
acftest <- start_event(dropdat, column = "survey_year", event = "ID_site")

#sample 80/20 for training and testing
data <- dropdat
ind <- sample(2, nrow(data), replace = TRUE, prob=c(0.7, 0.3))
train <- data[ind == 1,]
test <- data[ind == 2,]

#run model without rho, get start value from function below and then run
#acf term model
mod <- test.mod
mod <- N.bam.sel
acf(resid(mod))
#acf(resid_gam(mod), main="acf(resid_gam(N.bam.sel))")

r1 <- start_value_rho(mod, plot=TRUE)

# Make predictions
predictions <- mod %>% predict(test)
plot(predictions, test$N.y)
  
N.bam.sel <- bam(N.y ~ #s(n_nh4, bs = "ts") +
                   #s(n_no3, bs = "ts") +
                   te(n_nh4, n_no3, bs = "ts")+
                   #s(survey_year, bs= "ts")+
                   s(sum_canopy,bs="ts") +
                   #s(L.x, bs= "ts")+
                   #s(lagN, bs="ts")+
                   s(mean_temp,bs= "ts")+
                   s(mean_precip,bs="ts")+
                   #s(herb_layer,bs="ts")+
                   s(mean_age,bs ="ts",k=5)+
                    #as.factor(tree)+
                    #s(country, bs="re")+
                    s(ID_site,bs="re") +
                    s(survey_year,ID_site, bs="re"),
                    #te(longitude,latitude,survey_year,bs="ts"),
                    #rho=r1, AR.start=acftest$start.event,
                 cluster = cl,family = gaussian, data = dat.all.countries)

test.mod <- gam(list(N.y~#s(n_nh4, bs = "ts") +
                       #s(n_no3, bs = "ts") +
                       te(n_nh4, n_no3, bs = "ts")+
                       #s(survey_year, bs= "ts")+
                       s(sum_canopy,bs="ts") +
                       #s(L.x, bs= "ts")+
                       #s(lagN, bs="ts")+
                       s(mean_temp,bs= "ts")+
                       s(mean_precip,bs="ts")+
                       #s(herb_layer,bs="ts")+
                       s(mean_age,bs ="ts",k=5)+
                       #as.factor(tree)+
                       #s(country, bs="re")+
                       s(survey_year,ID_site, bs="re"), ~
                       #s(n_nh4, bs = "ts") +
                       #s(n_no3, bs = "ts") +
                       te(n_nh4, n_no3, bs = "ts")+
                       #s(survey_year, bs= "ts")+
                       s(sum_canopy,bs="ts") +
                       #s(L.x, bs= "ts")+
                       #s(lagN, bs="ts")+
                       s(mean_temp,bs= "ts")+
                       s(mean_precip,bs="ts")+
                       #s(herb_layer,bs="ts")+
                       s(mean_age,bs ="ts",k=5)+
                       #as.factor(tree)+
                       #s(country, bs="re")+
                       s(survey_year,ID_site, bs="re")),family=gaulss(), cluster =cl,
                data= dat)

summary(N.bam.sel)
b <- getViz(N.bam.sel)
plot_gam_check(b)
print(plot(b, allTerms = T), pages = 1)
gam.check(b)
vis.concurvity(b)

#GAMM€ gamm4(.....,random=c(1|year/site_id))

N.bam.sel <- bam(N.y/9 ~ #s(n_nh4, bs = "tp") +
                   #s(n_no3, bs = "tp") +
                   
                   # s(N, bs="tp",m=2)+
                   # s(N, bs="tp", by=tree, m=1)+
                   # s(tree, bs="re"),
                   
                    #s(n_no3, bs="tp",m=2)+
                    #s(n_no3, bs="tp", by=tree,m=1)+
                    #s(tree,bs="re"),
                    #s(country, bs="re"),
                   
                   te(n_nh4, n_no3, bs = "tp",m=2) +
                   t2(n_nh4, n_no3, tree, bs=c("tp","tp","re"), m=2, full=TRUE)+
                   
                   #s(sum_canopy,bs="tp") +
                   #s(L.x, bs= "tp")+
                   #te(longitude,latitude)+
                   #s(survey_year)+
                   #s(mean_temp,bs= "tp")+
                   #s(mean_precip,bs="tp")+
                   #s(mean_age,bs ="tp",k=6)+
                   s(lagN,bs="tp")+
                   #s(country, bs="re")+
                   as.factor(tree)+
                   te(longitude,latitude,survey_year,bs="tp"),
                 cluster = cl, family = betar, data = filter(dat.select.countries))

summary(N.bam.sel)
b <- getViz(N.bam.sel)
plot_gam_check(b)
print(plot(b, allTerms = T), pages = 1)
gam.check(b)
plot_gam_check(b)
vis.concurvity(b)


N.te.sel <-
  qgam(log(N.y) ~
         te(n_nh4, n_no3, bs = "ts") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         s(mean_temp,bs= "ts")+
         s(mean_precip,bs="ts")+
         s(mean_age,bs ="ts",k=3)+ 
         as.factor(tree)+
         te(longitude,latitude,survey_year,bs="ts"), cluster = cl,
       data = dat.select.countries , qu = 0.5)

summary(N.te.sel)         
b <- getViz(N.te.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
gam.check(b)
vis.concurvity(b)

#DIV####
div.sel <-
  qgam(log(div.y) ~ s(n_nh4, bs = "ts") +
         s(n_no3, bs = "ts") +
         ti(n_nh4, n_no3, bs = "ts") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         #s(lagdiv,bs="ts")+
         s(mean_temp,bs= "ts")+
         s(mean_precip,bs="ts")+
         s(mean_age,bs ="ts",k=3)+ 
         as.factor(tree)+
         te(longitude,latitude,survey_year,bs="ts"), cluster = cl,
       data = dat.select.countries , qu = 0.5)

summary(div.sel)         
b <- getViz(div.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
gam.check(b)
vis.concurvity(b)


div.bam.sel <- bam(div.y/4 ~ s(n_nh4, bs = "ts") +
                   s(n_no3, bs = "ts") +
                   #ti(n_nh4, n_no3, bs = "ts") +
                   #s(sum_canopy,bs="ts") +
                   #s(L.x, bs= "ts")+
                   s(lagdiv,bs="ts")+
                   #s(mean_temp,bs= "ts")+
                   #s(mean_precip,bs="ts")+
                   #s(mean_age,bs ="ts",k=3)+ 
                   as.factor(tree),
                   #te(longitude,latitude,survey_year,bs="ts"),
                 cluster = cl,family= betar, 
                 data = dat.select.countries)

summary(div.bam.sel)
b <- getViz(div.bam.sel)
plot_gam_check(b)
print(plot(b, allTerms = T), pages = 1)
gam.check(b)
vis.concurvity(b)


div.te.sel <-
  qgam(log(div.y) ~
         te(n_nh4, n_no3, bs = "ts") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         s(mean_temp,bs= "ts")+
         s(mean_precip,bs="ts")+
         s(mean_age,bs ="ts",k=3)+ 
         as.factor(tree)+
         te(longitude,latitude,survey_year,bs="ts"), cluster = cl,
       data = dat.select.countries, qu = 0.5)

summary(div.te.sel)         
b <- getViz(div.te.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
gam.check(b)
vis.concurvity(b)

#RAO####
Rao.sel <-
  qgam(RaoQ ~ s(n_nh4, bs = "ts") +
         s(n_no3, bs = "ts") +
         ti(n_nh4, n_no3, bs = "ts") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         #s(lagRao,bs="ts")+
         s(mean_temp,bs= "ts")+
         s(mean_precip,bs="ts")+
         s(mean_age,bs ="ts",k=3)+ 
         as.factor(tree)+
         te(longitude,latitude,survey_year,bs="ts"), cluster = cl,
       data = dat.select.countries , qu = 0.5)

summary(Rao.sel)         
b <- getViz(Rao.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
gam.check(b)
vis.concurvity(b)


Rao.bam.sel <- bam(RaoQ ~ s(n_nh4, bs = "ts") +
                   s(n_no3, bs = "ts") +
                   ti(n_nh4, n_no3, bs = "ts") +
                   s(sum_canopy,bs="ts") +
                   #s(L.x, bs= "ts")+
                   s(mean_temp,bs= "ts")+
                   s(mean_precip,bs="ts")+
                   s(mean_age,bs ="ts",k=3)+ 
                   as.factor(tree)+
                   te(longitude,latitude,survey_year,bs="ts"),
                 cluster = cl, data = dat.select.countries)

summary(Rao.bam.sel)
b <- getViz(Rao.bam.sel)
print(plot(b, allTerms = T), pages = 1)
gam.check(b)
plot_gam_check(b)
vis.concurvity(b)

rao.te.sel <-
  qgam(RaoQ ~
         te(n_nh4, n_no3, bs = "ts") +
         s(sum_canopy,bs="ts") +
         #s(L.x, bs= "ts",k=3)+
         s(mean_temp,bs= "ts")+
         s(mean_precip,bs="ts")+
         s(mean_age,bs ="ts",k=3)+ 
         as.factor(tree)+
         te(longitude,latitude,survey_year,bs="ts"), cluster = cl,
       data = dat.select.countries , qu = 0.5)

summary(div.te.sel)         
b <- getViz(div.te.sel)
print(plot(b, allTerms = T), pages = 1)
plot_gam_check(b)
check.qgam(b, nbin = 20)
gam.check(b)
vis.concurvity(b)
