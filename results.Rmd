---
title: "Results"
output:
  pdf_document: default
  html_document:
    df_print: paged
---


#### Mean ellenberg N value of bryophyte community 

```{r}
fit <- qgam1.7.bN
  
summary(fit)
  
b <- getViz(fit)
print(plot(b, allTerms = T), pages = 1) # Calls print.plotGam()
cqcheck(obj = fit, v = c("n_nh4"))
cqcheck(obj = fit, v = c("n_no3"))
cqcheck(obj = fit, v = c("n_nh4", "n_no3"), nbin = c(5, 5))
acf(resid(fit), main = "ACF")
  
  
```


```{r}

```
  

#### Mean Shannon diversity value of bryophyte community 

  
```{r}
fit <- gamm2.bd$gam
  
summary(fit)
  
b <- getViz(fit)
print(plot(b, allTerms = T), pages = 1) # Calls print.plotGam()
cqcheck(obj = fit, v = c("n_nh4"))
cqcheck(obj = fit, v = c("n_no3"))
cqcheck(obj = fit, v = c("n_nh4", "n_no3"), nbin = c(5, 5))
acf(resid(fit), main = "ACF")

```

 
 
#### Mean Ellenberg R value of bryophyte community

```{r}
fit <- gam2.br2
  
summary(fit)
  
b <- getViz(fit)
print(plot(b, allTerms = T), pages = 1) # Calls print.plotGam()
cqcheck(obj = fit, v = c("n_nh4"))
cqcheck(obj = fit, v = c("n_no3"))
cqcheck(obj = fit, v = c("n_nh4", "n_no3"), nbin = c(5, 5))
acf(resid(fit), main = "ACF")()
```


#### Species richness of bryophyte community 

```{r}
fit <- gam2.br2
  
summary(fit)
 
b <- getViz(fit)
print(plot(b, allTerms = T), pages = 1) # Calls print.plotGam()
cqcheck(obj = fit, v = c("n_nh4"))
cqcheck(obj = fit, v = c("n_no3"))
cqcheck(obj = fit, v = c("n_nh4", "n_no3"), nbin = c(5, 5))
acf(resid(fit), main = "ACF")
```

#### CWM Rao Quadratic Entropy

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.

```{r}
fit <- gam2.br2
  
summary(fit)

b <- getViz(fit)
print(plot(b, allTerms = T), pages = 1) # Calls print.plotGam()
cqcheck(obj = fit, v = c("n_nh4"))
cqcheck(obj = fit, v = c("n_no3"))
cqcheck(obj = fit, v = c("n_nh4", "n_no3"), nbin = c(5, 5))
acf(resid(fit), main = "ACF")
```

