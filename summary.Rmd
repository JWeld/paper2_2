---
title: "Summary"
output: html_notebook
---
The dataset used is a combination of foliar chemistry, deposition chemistry, and vegetation survey data (both vascular and non-vascular plants) from ICP Forests and ICP IM. A limitation is that not all sites collected data for all required variables.

N deposition (combined NH4 and NO3) is positively correlated with canopy N:P ratio in plots where the dominant canopy species is a conifer, less clear relationship in broadleaves.

```{r}
ggplot(filter(dat1, N_dep <= 5), aes(x=N_dep, y= N.P,  colour = grp_tree_species))+ geom_point() + 
    geom_smooth(method = "loess")+ theme_grey() + labs(y= "N:P ratio canopy foliage", x = "N_dep")
```

Positive correlation between N:P ratio and canopy cover, apart from at the highest values in broadleaves.

```{r}
ggplot(filter(dat1, sum_canopy <= 200), aes(x=N.P, y= sum_canopy, colour = grp_tree_species))+ geom_point() + 
    geom_smooth(method = "loess")+ theme_grey() + labs(y= "sum_canopy", x = "N:P ratio canopy foliage")
```

This suggests (as others have proposed) that N deposition and/or increased CO2 levels have resulted in denser canopy growth and changes in nutrient balances. But how does this affect understorey vegetation?


Is there a relationship between canopy N:P and understorey (vascular) N:P? Yes, positive correlation, stronger for conifer dominated sites. (although note that this is based on a small subset of the canopy N:P data where understorey N:P data were also available - much less extensive measurements than canopy foliar chemistry).
```{r}
ggplot(tem2, aes(x=N.P.x, y= N.P.y, colour = grp_tree_species))+ geom_point() + 
  geom_smooth(method = "loess")+ theme_grey() + labs(x = "N.P ratio canopy", y = "N.P ratio understorey")
```
This suggests that
