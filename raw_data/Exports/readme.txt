Still needed- a metric/proxy for shade level or canopy growth

May change- the CWM Ellenberg values have alternative ways of generating them
and some species are still unassigned due to confusing taxonomy.
They probably won't chnage very greatly though.

foliar_chemistry XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ID is a combination of year, country and site
n is nitrogen level
p is phosphorus level
N.P is the nitrogen/phosphorus ration
code_leaves_type is age of leaf/needle (0 current year, 1 previous year etc)
grp_tree_species categorises trees by conifer/broadleaf



deposition XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ID is a combination of year, country and site
n_nh4 is NH4 deposition
n_no3 is NO3 deposition


vegetation XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ID is a combination of year, country and site
ID_fine is a combination of year, country, site and plot number
cover is percentage cover 

bryophytes_CWM_ell XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

CWM_ell is mean community weighted value for Ellenberg values (at ID level)
L is light preference
F is moisture preference (from Feuchtigkeit)
R is pH (reaction) preference
N is nitrogen/nutrient preference
T and K can be ignored for now


vascular_plants_CWM_ell XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

CWM_ell is mean community weighted value for Ellenberg values (at ID level)
L is light preference
F is moisture preference (from Feuchtigkeit)
R is pH (reaction) preference
N is nitrogen/nutrient preference


