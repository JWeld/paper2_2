#gam
gam1 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
                   ti(n_no3, bs = "cs") +
                   ti(n_nh4, n_no3, bs = "cs") +
                   s(sum_canopy, bs = "cs") + 
                   s(L.x, bs= "cs")+
                   s(mean_temp, bs = "cs") +  
                   s(mean_precip, bs = "cs") +
                   te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
                 data = dat1)

summary(gam1 )
draw(gam1 )
appraise(gam1 )

gam2 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              s(sum_canopy, bs = "cs") + 
              s(L.x, bs= "cs")+
              s(mean_temp, bs = "cs", k=6) +  
              s(mean_precip, bs = "cs", k=6) +
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1)

summary(gam2)
draw(gam2)
appraise(gam2)

gam3 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              s(sum_canopy, bs = "cs") + 
              s(L.x, bs= "cs")+
              s(mean_temp, bs = "cs", k=6) +  
              s(mean_precip, bs = "cs", k=6) +
              s(tree, bs="re")+
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1)

summary(gam3)
draw(gam3)
appraise(gam3 )

gam4 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              #s(sum_canopy, bs = "cs") + 
              s(L.x, bs= "cs")+
              s(mean_temp, bs = "cs", k=6) +  
              s(mean_precip, bs = "cs", k=6) +
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1)

summary(gam4 )
draw(gam4 )
appraise(gam4 )

gam5 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              #s(sum_canopy, bs = "cs") + 
              #s(L.x, bs= "cs")+
              #s(mean_temp, bs = "cs", k=6) +  
              #s(mean_precip, bs = "cs", k=6) +
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1)

summary(gam5 )
draw(gam5 )
appraise(gam5 )

qgam5 <- qgam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              #s(sum_canopy, bs = "cs") + 
              #s(L.x, bs= "cs")+
              #s(mean_temp, bs = "cs", k=6) +  
              #s(mean_precip, bs = "cs", k=6) +
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1, qu=0.5)
summary(qgam5)
draw(qgam5)
check.qgam(qgam5, nbin = 20)

gam6 <- gam(RaoQ ~ ti(n_nh4, bs = "cs") +
              ti(n_no3, bs = "cs") +
              ti(n_nh4, n_no3, bs = "cs") +
              s(sum_canopy, bs = "cs") + 
              s(L.x, bs= "cs")+
              s(mean_temp, bs = "cs", k=6) +  
              s(mean_precip, bs = "cs", k=6) +
              te(latitude, longitude, survey_year, d = c(2, 1), bs = c("cs", "cs"), k = c(30, 8)),
            data = dat1)

summary(gam6 )
draw(gam6 )
appraise(gam6 )

AIC(gam1,gam2,gam3,gam4,gam5,qgam5,gam6)
